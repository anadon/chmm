
#include <array>
#include <vector>
#include "include/hmm.hpp"


using std::array;
using std::vector;


enum class coin_type {
    fair,
    biased
};
//using enum coin_type;
enum class coin_side {
    heads,
    tails
};
//using enum coin_side;
const static array<double, 2>
initial_state_test = {0.5, 0.5};
//----------------+----------------+
// P(F_n|F_{n-1}) | P(B_n|F_{n-1}) |
//----------------+----------------+
// P(F_n|B_{n-1}) | P(B_n|B_{n-1}) |
//----------------+----------------+
const static array<
array<double, 2>,
      2>
transision_probabilities = {
    array<double, 2>({0.9, 0.1}),
    array<double, 2>({0.1, 0.9})
};

//--------+--------+
// P(H|F) | P(T|F) |
//--------+--------+
// P(H|B) | P(T|B) |
//--------+--------+

const static array<
array<double, 2>,
      2>
emission_probabilities = {
    array<double, 2>({0.5, 0.75}),
    array<double, 2>({0.5, 0.25})
};


int hmm_test_wrapper(auto observations, auto expected_coin_type_prediction) {

    assert(expected_coin_type_prediction.size() == observations.size());

    vector comparable_obs(observations.begin(), observations.end());
    vector expected_cointype(expected_coin_type_prediction.begin(), expected_coin_type_prediction.end());


    auto result = hmm<coin_type, coin_side, double>(initial_state_test, transision_probabilities, emission_probabilities, observations);

    assert(result.size() == observations.size());

    assert(expected_cointype == result);

    return 0;
}


int main(){
        array
    observed_coin_toss_series = {
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::tails,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads,
        coin_side::heads
    };

    array
    expected_hidden_states = {
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::fair,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased,
        coin_type::biased
    };

    hmm_test_wrapper(observed_coin_toss_series, expected_hidden_states);

    return 0;
}